#!/usr/bin/env python3
from setuptools import setup

setup(
    name='filesystem-cleanup',
    version='0.1',
    packages=['src'],
    package_dir={'src': 'src'},
    scripts=['src/main.py'],
    test_suite='tests',
    setup_requires=['pytest-runner'],
    install_requires=[],
    tests_require=['pytest'],
    url='https://gitlab.com/alcibiade/filesystem-cleanup',
    license='MIT',
    author='Yannick Kirschhoffer',
    author_email='alcibiade@alcibiade.org',
    description='A set of python scripts helping make sense of busy media archives filesystems.',
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'Intended Audience :: System Administrators',
        'Topic :: System :: Systems Administration',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.7',
    ],
)
