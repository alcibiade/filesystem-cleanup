import unittest

from fsreader import FilesystemReader


class TestSampleFolder(unittest.TestCase):

    def test_read(self):
        reader = FilesystemReader()
        analytics = reader.read_files("samples")
        self.assertEqual(3, len(analytics.files))
        self.assertEqual(2, len(analytics.get_aggregated_content().keys()))
        self.assertEqual(2, len(analytics.get_folders()))


if __name__ == '__main__':
    unittest.main()
