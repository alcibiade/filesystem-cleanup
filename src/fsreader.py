import logging
import os

from hash import HashGenerator
from model import FileInformation, FilesystemAnalytics

_logger = logging.getLogger(__name__)


class FilesystemReader:

    def __init__(self):
        self._hashgenerator = HashGenerator()

    def read_files(self, search_folder: str) -> FilesystemAnalytics:
        all_files = []

        for folder_name, sub_directories, local_files in os.walk(search_folder):
            for local_file_name in local_files:
                file_path_full = os.path.normpath(folder_name + '/' + local_file_name)
                file_size = os.stat(file_path_full).st_size
                file_hash = self._hashgenerator.compute_hash(file_path_full)

                all_files.append(FileInformation(file_path_full, file_size, file_hash, folder_name))
                _logger.info('%8d: %s' % (len(all_files), file_path_full))

        return FilesystemAnalytics(files=all_files)
