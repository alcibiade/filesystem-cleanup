#!/usr/bin/env python3
import argparse
import logging

from fsreader import FilesystemReader

_logger = logging.getLogger(__name__)

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    parser = argparse.ArgumentParser()
    parser.add_argument('folder')
    namespace = parser.parse_args()

    reader = FilesystemReader()
    analytics = reader.read_files(namespace.folder)

    _logger.info(f'Found {len(analytics.files)} files in {namespace.folder}')

    contents = analytics.get_aggregated_content()

    for content, files in contents.items():
        if len(files) > 1:
            _logger.info(f'Found duplicates: {files}')
