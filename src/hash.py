import hashlib


class HashGenerator:
    def __init__(self, byteslen: int = 4096):
        self._byteslen = byteslen

    def compute_hash(self, f: str) -> str:
        md5_hash = hashlib.md5()

        with open(f, "rb") as f:
            block = f.read(self._byteslen)
            md5_hash.update(block)

        return md5_hash.hexdigest()
