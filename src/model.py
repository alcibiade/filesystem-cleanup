from typing import List, Dict


class ContentInformation:
    def __init__(self, hash: str, size: int):
        self.hash = hash
        self.size = size

    def __eq__(self, other):
        return self.hash == other.hash and self.size == other.size

    def __hash__(self):
        return hash(self.hash)


class Folder:
    def __init__(self, path):
        self.path = path

    def __eq__(self, other):
        return self.path == other.path

    def __hash__(self):
        return hash(self.path)

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return f'Folder({self.path})'

class FileInformation:
    def __init__(self, path: str, size: int, hash: str, folder: str):
        self.path = path
        self.size = size
        self.hash = hash
        self.folder = Folder(path=folder)

    def content(self) -> ContentInformation:
        return ContentInformation(self.hash, self.size)

    def __eq__(self, other):
        return self.path == other.path

    def __hash__(self):
        return hash(self.path)

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return f'FileInformation({self.path})'


class FilesystemAnalytics:
    def __init__(self, files: List[FileInformation]):
        self.files = files

    def get_aggregated_content(self) -> Dict[ContentInformation, List[FileInformation]]:
        contents = {}
        for f in self.files:
            content = f.content()
            if content not in contents:
                contents[content] = []

            contents[content].append(f)
        return contents

    def get_folders(self) -> Dict[Folder, List[FileInformation]]:
        folders = {}

        for f in self.files:
            if f.folder not in folders.keys():
                folders[f.folder] = []

            folders[f.folder].append(f)

        return folders
